#!/usr/bin/python

import sys
from numpy import average, std, nan
import pandas
import matplotlib.pyplot as plt
import glob
from scipy.stats import pearsonr,spearmanr
from math import e,log 
import subprocess

#################################################################################
####  GLOBAL DATA ###############################################################
#################################################################################

obs = { "slaughterhouse" : 0.26, "aircraft" : 0.63, "callcenter" : 0.44, "restaurant" : 0.45, "smallbus" : 0.12, "bigbus" : 0.17, "choir" : 0.87 }
labels = { "slaughterhouse" : "Slaughterhouse", "aircraft" : "Aircraft", "callcenter" : "Call center", "restaurant" : "Resturant", "smallbus" : "Small bus", "bigbus" : "Big bus", "choir" : "Choir" }

#################################################################################
####  FIGURES ###################################################################
#################################################################################

#################################################################################
# FIGURA 6
# EXPERIMENTO: paper_school
# RESULTADOS: out_school
# build_bins: N= 168; G= 7; C= 1 2 3 4 5 6 7 8 
#################################################################################

def Fig5A():
    exp_results = pandas.read_csv("out_school/break.csv", low_memory=False)
    data = {}
    series = []
    for idx, rec in exp_results.iterrows():
        serie = rec.loc["fE"]
        X = rec.loc["BREAK_ENHANCEMENT_FACTOR"]
        Y = rec.loc["SECONDARY_CASES"]
        NSUS = rec.loc["NSUS"]
        if serie not in data: data[serie] = {}
        if X not in data[serie]: data[serie][X] = []
        data[serie][X].append( Y / float(NSUS) )
        series.append(serie)

    series = sorted(set(series))

    fig = plt.figure(figsize=(5,5))
    ax = fig.add_subplot()
    plt.rcParams.update({'font.size': 16})
    for serie in series:
        plotting = sorted([ ( float(x), average(y), std(y) ) for x,y in data[serie].items() ], key = lambda x: x[0])
        x, y, e = zip(*plotting)
        plt.errorbar(x=x, y=y, yerr=e, label="$f_i=f_e={}$".format(serie))

    ax.set_aspect(250)
    plt.xlabel("$r_E = r_B$")
    plt.ylabel("Attack Rate\n($AR^S$ - retQSS)")
    plt.xticks([1,5,10,15,20,25,30],fontsize=14)
    plt.yticks([0,0.025,0.05,0.075,0.1],fontsize=14)
    plt.ylim((0,0.12))
    plt.grid(color='black', linestyle='dashed', linewidth=0.3)
    plt.legend(loc="upper left",prop={'size': 8})
    plt.tight_layout()
    plt.savefig("F5A.png")

def Fig5B():
    exp_results = pandas.read_csv("out_school/different.csv", low_memory=False)
    data = {}
    series = []
    for idx, rec in exp_results.iterrows():
        serie = rec.loc["DIFFERENT_CLASSROOMS"]
        X = rec.loc["VOLUME_DIFFERENT_CLASSROOM_CONCENTRATION_LAMBDA"]
        Y = rec.loc["SECONDARY_CASES"]
        NSUS = rec.loc["NSUS"]
        if serie not in data: data[serie] = {}
        if X not in data[serie]: data[serie][X] = []
        data[serie][X].append( Y / float(NSUS) )
        series.append(serie)

    series = sorted(set(series))

    fig = plt.figure(figsize=(5,5))
    ax = fig.add_subplot()
    plt.rcParams.update({'font.size': 16})

    for serie in series:
        plotting = sorted([ ( float(x), average(y), std(y) ) for x,y in data[serie].items() ], key = lambda x: x[0])
        x, y, e = zip(*plotting)
        plt.errorbar(x=x, y=y, yerr=e, label="#IVC={}".format(serie))

    ax.set_aspect(5/0.025)
    plt.xlabel("$\lambda_{IVC}$")
    plt.ylabel("Attack Rate\n($AR^S$ - retQSS)")
    plt.xlim((0.5,5.5))
    plt.ylim((0.005,0.03))
    plt.xticks([1,2,3,4,5],fontsize=14)
    plt.yticks([0.005,0.01,0.015,0.02,0.025,0.03],fontsize=14)
    plt.grid(color='black', linestyle='dashed', linewidth=0.3)
    plt.legend(loc="upper right",prop={'size': 10})
    plt.tight_layout()
    plt.savefig("F5B.png")

#################################################################################
# FIGURA 4
# EXPERIMENTO: paper_school
# RESULTADOS: out_school
# build_bins: N= 21 47 18 61 217 20 87; G= 7 
#################################################################################

def Fig4A():
    exp_results = pandas.read_csv("out_school/peng.csv", low_memory=False)
    data = {}
    series = []
    for idx, rec in exp_results.iterrows():
        serie = rec.loc["CASE_STUDY"]
        X = rec.loc["D"]
        Y = rec.loc["SECONDARY_CASES"]
        NSUS = rec.loc["NSUS"]
        if serie not in data: data[serie] = {}
        if X not in data[serie]: data[serie][X] = []
        data[serie][X].append( Y / float(NSUS) )
        series.append(serie)

    print( "POST-NOBREAK", ( average(data["Peng_post-NOBREAK"][2] ) + average(data["Peng_post-NOBREAK"][2] ) ) / 2.0 )
    print( "PRE-NOBREAK", average(data["Peng_pre-NOBREAK"][5] ) )

    plt.rcParams.update({'font.size': 16})
    fig = plt.figure(figsize=(5,5))
    ax = fig.add_subplot()

    series = ["pre-NOBREAK","pre-CLOSED","pre-OPEN","post-NOBREAK","post-CLOSED","post-OPEN"]#sorted(set(series))
    for serie in series:
        plotting = sorted([ ( float(x), average(y), std(y) ) for x,y in data["Peng_"+serie].items() ], key = lambda x: x[0])
        x, y, e = zip(*plotting)
        #print(serie, x, y)
        plt.errorbar(x=x, y=y, yerr=e, label="Classroom "+serie.upper().replace("-"," (")+")")

    axes=plt.gca()
    axes.set_aspect(5.5/0.025)
    plt.xlabel("Duration (D)")
    plt.ylabel("Attack Rate\n($AR^S$ - retQSS)")
    plt.xticks([1,2,3,4,5,6],fontsize=14)
    plt.yticks([0,0.005,0.01,0.015,0.02,0.025],fontsize=14)
    plt.grid(color='black', linestyle='dashed', linewidth=0.3)
    plt.ylim(0,0.025)
    plt.legend(loc="upper left",prop={'size': 7})
    plt.tight_layout()
    plt.savefig("F4A.png")

def Fig4B():
    exp_results = pandas.read_csv("out_school/kurnitski.csv", low_memory=False)
    data = {}
    series = []
    for idx, rec in exp_results.iterrows():
        serie = rec.loc["CASE_STUDY"]
        X = rec.loc["D"]
        Y = rec.loc["SECONDARY_CASES"]
        NSUS = rec.loc["NSUS"]
        if serie not in data: data[serie] = {}
        if X not in data[serie]: data[serie][X] = []
        data[serie][X].append( Y / float(NSUS) )
        series.append(serie)

    print( "32m2-NOBREAK", average(data["Kurnitski_32m2-NOBREAK"][6] ) )
    print( "48m2-NOBREAK", average(data["Kurnitski_48m2-NOBREAK"][6] ) )
        
    plt.rcParams.update({'font.size': 16})
    fig = plt.figure(figsize=(5,5))
    ax = fig.add_subplot()

    series = ["32m2-NOBREAK","32m2-CLOSED","32m2-OPEN","48m2-NOBREAK","48m2-CLOSED","48m2-OPEN"]#sorted(set(series))
    for serie in series:
        plotting = sorted([ ( float(x), average(y), std(y) ) for x,y in data["Kurnitski_"+serie].items() ], key = lambda x: x[0])
        x, y, e = zip(*plotting)
        #print(serie, x, y)
        plt.errorbar(x=x, y=y, yerr=e, label="Classroom "+serie.upper().replace("M2","$m^2$").replace("-"," (")+")")

    axes=plt.gca()
    axes.set_aspect(5.5/0.025)
    plt.xlabel("Duration (D)")
    plt.ylabel("$AR^S$")
    plt.ylabel("Attack Rate\n($AR^S$ - retQSS)")
    plt.xticks([1,2,3,4,5,6],fontsize=14)
    plt.yticks([0,0.005,0.01,0.015,0.02,0.025],fontsize=14)
    plt.ylim(0,0.025)
    plt.grid(color='black', linestyle='dashed', linewidth=0.3)
    plt.legend(loc="upper left",prop={'size': 7})
    plt.tight_layout()
    plt.savefig("F4B.png")

#################################################################################
#################################################################################
# FIGURA A1
# EXPERIMENTO: paper_fitEp0
# RESULTADOS: out_fit
# build_bins: N= 21 47 18 61 217 20 87; G= 1 
#################################################################################

def FigA1():
    Dir = "out_fit"
    curves = {}
    exps = []
    for csv in glob.glob("{}/*csv".format(Dir)):
        exp_results = pandas.read_csv(csv, low_memory=False)
        exp = csv.replace("{}/".format(Dir),"").replace(".csv","")
        a = list(exp_results.loc[:,"EP0"])
        b = list(exp_results.loc[:,"SECONDARY_CASES"])
        c = list(exp_results.loc[:,"NSUS"])
        ep0s = sorted(set(a))
        curves[exp] = { ep0:average([ float(sc)/nsus for (e,sc,nsus) in zip(a,b,c) if e == ep0 ]) for ep0 in ep0s }
        exps.append(exp)

    plt.rcParams.update({'font.size': 18})
    fig = plt.figure(figsize=(6,5))
    ax = fig.add_subplot()

    for exp in curves:
        print(exp, [ (e,curves[exp][e]) for e in range(40,80) ])


    ep0s = sorted(curves[exps[0]])
    plt.plot(ep0s, [average( [ (curves[exp][ep0]-obs[exp])**2 for exp in exps ] ) for ep0 in ep0s ], label="MSE", linewidth=0.5, color="blue")
    MSEs = sorted(zip(ep0s, [average( [ (curves[exp][ep0]-obs[exp])**2 for exp in exps ] ) for ep0 in ep0s ] ), key=lambda x: x[1] )
    print(MSEs[:5])
    smoothed = [average( [ (curves[exp][ep0]-obs[exp])**2 for exp in exps ] ) for ep0 in ep0s ]
    smoothed = [ average( smoothed[i-5:i+6] ) for i in range(5,len(smoothed)-6) ]
    plt.plot(ep0s[5:-6], smoothed, label="MSE (smoothed)", linewidth=1.5, color="blue")
    #print(sorted(zip(ep0s[5:-6], smoothed), key=lambda x: x[1]))
    smoothedMSE = sorted(zip(ep0s[5:-6], smoothed), key=lambda x: x[1])

    plt.xlim((0,300))
    plt.ylim((0,0.1))
    plt.xticks([0,50,100,150,200,250,300],fontsize=14)
    plt.yticks([0,0.02,0.04,0.06,0.08,0.1],fontsize=14)
    plt.xlabel("Ep0")
    plt.ylabel("MSE")
    plt.grid(color='black', linestyle='dashed', linewidth=0.3)

    ax2=ax.twinx()
    ax2.set_ylim((0,1))
    ax2.plot(nan, label = 'MSE', color="b", linewidth=0.5)  # Make an agent in ax
    ax2.plot(nan, label = 'MSE (smoothed)', color="b", linewidth=1.5)  # Make an agent in ax
    ax2.plot(ep0s, [average( [ abs(curves[exp][ep0]-obs[exp])/obs[exp] for exp in exps ] ) for ep0 in ep0s ], label="MRE", color="r", linewidth=0.5)
    MREs = sorted(zip(ep0s, [average( [ abs(curves[exp][ep0]-obs[exp])/obs[exp] for exp in exps ] ) for ep0 in ep0s ] ), key=lambda x: x[1])
    print(MREs[:5])
    smoothed = [average( [ abs(curves[exp][ep0]-obs[exp])/obs[exp] for exp in exps ] ) for ep0 in ep0s ]
    smoothed = [ average( smoothed[i-5:i+6] ) for i in range(5,len(smoothed)-6) ]
    ax2.plot(ep0s[5:-6], smoothed, label="MRE (smoothed)", color="r", linewidth=1.5)
    #print(sorted(zip(ep0s[5:-6], smoothed), key=lambda x: x[1]))
    smoothed = sorted(zip(ep0s[5:-6], smoothed), key=lambda x: x[1])

    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    ax2.set_ylabel("MRE")

    ax2.legend(loc="upper right",prop={'size': 12})
    plt.tight_layout()
    plt.savefig("FA1.png")

    MSE_min = min(smoothedMSE, key = lambda x: x[1])[1]
    MSE_max = max(smoothedMSE, key = lambda x: x[1])[1]
    MRE_min = min(smoothed, key = lambda x: x[1])[1]
    MRE_max = max(smoothed, key = lambda x: x[1])[1]

    Norm = []
    #for ep01se, ep02re in zip(MSEs, MREs):
    for ep01se, ep02re in zip(smoothedMSE, smoothed):
        se = (ep01se[1]-MSE_min)/(MSE_max-MSE_min)
        re = (ep02re[1]-MRE_min)/(MRE_max-MRE_min)
        ep0 = ep01se[0]
        Norm.append((ep0, (se+re)/2.0))

    Norm.sort(key = lambda x: x[1])
    print()
    #print(Norm)

def FigA2A():
    Dir = "out_fit"
    curves = {}
    exps = []
    for csv in glob.glob("{}/*csv".format(Dir)):
        exp_results = pandas.read_csv(csv, low_memory=False)
        exp = csv.replace("{}/".format(Dir),"").replace(".csv","")

        a = list(exp_results.loc[:,"EP0"])
        b = list(exp_results.loc[:,"SECONDARY_CASES"])
        c = list(exp_results.loc[:,"NSUS"])

        ep0s = sorted(set(a))

        curves[exp] = { ep0:average([ float(sc)/nsus for (e,sc,nsus) in zip(a,b,c) if e == ep0 ]) for ep0 in ep0s }

        exps.append(exp)

    plt.rcParams.update({'font.size': 18})
    fig = plt.figure(figsize=(5,4))
    ax = fig.add_subplot()

    for exp_out in exps:
        ep0s = sorted(curves[exp_out])
        plt.plot(ep0s, [average( [ (curves[exp][ep0]-obs[exp])**2 for exp in exps if not exp == exp_out ] ) for ep0 in ep0s ], label=labels[exp_out])
        mseloo = [average( [ (curves[exp][ep0]-obs[exp])**2 for exp in exps if not exp == exp_out ] ) for ep0 in ep0s ]
        print(exp_out, sorted(zip(ep0s, mseloo), key=lambda x: x[1] )[:5])

    ax.set_aspect(300/0.1)
    plt.xlim((0,300))
    plt.ylim((0,0.1))
    plt.xticks([0,50,100,150,200,250,300],fontsize=14)
    plt.yticks([0,0.02,0.04,0.06,0.08,0.1],fontsize=14)
    plt.grid(color='black', linestyle='dashed', linewidth=0.3)

    plt.xlabel("Ep0")
    plt.ylabel("MSE")
    plt.legend(loc="lower right",prop={'size': 5})
    plt.tight_layout()
    plt.savefig("FA2A.png")

def FigA2B():
    Dir = "out_fit"
    curves = {}
    exps = []
    for csv in glob.glob("{}/*csv".format(Dir)):
        exp_results = pandas.read_csv(csv, low_memory=False)
        exp = csv.replace("{}/".format(Dir),"").replace(".csv","")

        a = list(exp_results.loc[:,"EP0"])
        b = list(exp_results.loc[:,"SECONDARY_CASES"])
        c = list(exp_results.loc[:,"NSUS"])

        ep0s = sorted(set(a))

        curves[exp] = { ep0:average([ float(sc)/nsus for (e,sc,nsus) in zip(a,b,c) if e == ep0 ]) for ep0 in ep0s }

        exps.append(exp)

    plt.rcParams.update({'font.size': 18})
    fig = plt.figure(figsize=(5,4))
    ax = fig.add_subplot()

    for exp_out in exps:
        ep0s = sorted(curves[exp_out])
        plt.plot(ep0s, [average( [ abs(curves[exp][ep0]-obs[exp])/obs[exp] for exp in exps if not exp == exp_out ] ) for ep0 in ep0s ], label=labels[exp_out])
        mreloo = [average( [ abs(curves[exp][ep0]-obs[exp])/obs[exp] for exp in exps if not exp == exp_out ] ) for ep0 in ep0s ]
        print(exp_out, sorted(zip(ep0s, mreloo), key=lambda x: x[1] )[:5])

    ax.set_aspect(300)
    plt.xlim((0,300))
    plt.ylim((0,1.0))
    plt.xticks([0,50,100,150,200,250,300],fontsize=14)
    plt.yticks(fontsize=14)
    plt.grid(color='black', linestyle='dashed', linewidth=0.3)

    plt.xlabel("Ep0")
    plt.ylabel("MRE")
    plt.legend(loc="lower right",prop={'size': 5})
    plt.tight_layout()
    plt.savefig("FA2B.png")

#################################################################################
# FIGURA 3
# EXPERIMENTO: paper_peng, paper_kurnitski
# RESULTADOS: out_peng, out_kurnitski
# build_bins peng: N= 2 3 4 7 11 13 16 21 26 31 51 61 84 101 151 201 225 251 301 501 1001 2100; G= 1
# build_bins kurnitski: N= 2 6 8 10 13 17 24 25 154 ; G= 1
#################################################################################

def Fig3A():
    points_x = []
    points_y = []
    points_yerr = []
    points_type = []
    cases = []
    for csv in glob.glob("out_peng/*csv"):
        exp_results = pandas.read_csv(csv, low_memory=False)
    
        a = list(exp_results.loc[:,"SECONDARY_CASES"])
        b = list(exp_results.loc[:,"secondary_cases"])
        c = list(exp_results.loc[:,"NSUS"])

        cases.append( (csv, average(a) / average(c), average(b) / average(c)) )
        points_y.append( average(a) / average(c) )
        points_yerr.append( std(a / average(c) ) )
        points_x.append( average(b) / average(c) )

        if "airport" in csv:
            p_type = "AIR"
        elif csv.replace("out_peng/","").startswith("classroom"):
            p_type = "CLASS"
            print("CLASS", csv, average(a) / average(c), average(b) / average(c))
        else:
            p_type = "OTHER"

        if "pre" in csv:
            p_type+= "-PRE"
        else:
            p_type+= "-POST"
            
        points_type.append( p_type )

    #print(cases)
    #print(points_x,points_y)
    #print(pearsonr(points_x,points_y))
    #print(spearmanr(points_x,points_y))

    #print(list(zip(points_y,points_type)))

    pres_y_other = [ y for y,t in zip(points_y,points_type) if "PRE" in t and not "AIR" in t and not "CLASS" in t ]
    pres_x_other = [ x for x,t in zip(points_x,points_type) if "PRE" in t and not "AIR" in t and not "CLASS" in t ]
    posts_y_other = [ y for y,t in zip(points_y,points_type) if "POST" in t and not "AIR" in t and not "CLASS" in t ]
    posts_x_other = [ x for x,t in zip(points_x,points_type) if "POST" in t and not "AIR" in t and not "CLASS" in t ]

    pres_y_air = [ y for y,t in zip(points_y,points_type) if "PRE" in t and "AIR" in t ]
    pres_x_air = [ x for x,t in zip(points_x,points_type) if "PRE" in t and "AIR" in t ]
    posts_y_air = [ y for y,t in zip(points_y,points_type) if "POST" in t and "AIR" in t ]
    posts_yerr_air = [ y for y,t in zip(points_yerr,points_type) if "POST" in t and "AIR" in t ]
    posts_x_air = [ x for x,t in zip(points_x,points_type) if "POST" in t and "AIR" in t ]

    pres_y_class = [ y for y,t in zip(points_y,points_type) if "PRE" in t and "CLASS" in t ]
    pres_x_class = [ x for x,t in zip(points_x,points_type) if "PRE" in t and "CLASS" in t ]
    posts_y_class = [ y for y,t in zip(points_y,points_type) if "POST" in t and "CLASS" in t ]
    posts_x_class = [ x for x,t in zip(points_x,points_type) if "POST" in t and "CLASS" in t ]

    plt.rcParams.update({'font.size': 15})
    fig = plt.figure(figsize=(4,4))
    ax = fig.add_subplot()
    plt.scatter(pres_x_other, pres_y_other,s=15, marker = 'o', color = "tab:blue", label = "Other Scenarios (PRE)")
    plt.scatter(posts_x_other, posts_y_other,s=15, marker = 'o', color = "tab:orange", label = "Other Scenarios (POST)")
    plt.scatter(pres_x_air, pres_y_air,s=15, marker='^',color="tab:blue", label = "Airport (PRE)",edgecolors="black",linewidth=.5)
    plt.scatter(posts_x_air, posts_y_air,s=15, marker='^',color="tab:orange", label = "Airport (POST)",edgecolors="black",linewidth=.5)
    plt.scatter(pres_x_class, pres_y_class,s=50,marker='*',color="tab:blue", label = "Classroom (PRE)",edgecolors="black",linewidth=.5)
    plt.scatter(posts_x_class, posts_y_class,s=50,marker='*',color="tab:orange", label = "Classroom (POST)",edgecolors="black",linewidth=.5)
    plt.plot([0,1000], [0,1000], linestyle='dashed', linewidth=0.5, color="black")

    ax.set_aspect('equal', adjustable='box')
    plt.yscale("log")
    plt.xscale("log")
    plt.ylim((0.0001,2))
    plt.xlim((0.0001,2))
    plt.xticks([0.0001, 0.001, 0.01, 0.1, 1], fontsize=12)
    plt.yticks([0.0001, 0.001, 0.01, 0.1, 1], fontsize=12)
    plt.grid(color='black', linestyle='dashed', linewidth=0.3)
    plt.xlabel("Attack Rate\n(Peng et al.)", fontsize=13)
    plt.ylabel("Attack Rate\n($AR^1$ - retQSS)")
    plt.legend(loc="lower right",prop={'size': 5.5})
    plt.tight_layout()
    plt.savefig("F3A.png")

def Fig3B():
    points_x = []
    points_y = []
    points_type = []
    cases = []
    for csv in glob.glob("out_kurnitski/*csv"):
        exp_results = pandas.read_csv(csv, low_memory=False)
    
        a = list(exp_results.loc[:,"SECONDARY_CASES"])
        b = list(exp_results.loc[:,"secondary_cases"])
        c = list(exp_results.loc[:,"NSUS"])

        cases.append( (csv, average(a) / average(c), average(b) / average(c)) )
        points_y.append( average(a) / average(c) )
        points_x.append( average(b) )
        
        if csv.replace("out_kurnitski/","").startswith("classroom"):
            p_type = "CLASS"
            print("CLASS", csv, average(a) / average(c), average(b))
        else:
            p_type = "OTHER"

        points_type.append(p_type)

    #print(cases)
    #print(points_x,points_y)
    #print(pearsonr(points_x,points_y))
    #print(spearmanr(points_x,points_y))

    pres_y_other = [ y for y,t in zip(points_y,points_type) if not "CLASS" in t ]
    pres_x_other = [ x for x,t in zip(points_x,points_type) if not "CLASS" in t ]
    posts_y_class = [ y for y,t in zip(points_y,points_type) if "CLASS" in t ]
    posts_x_class = [ x for x,t in zip(points_x,points_type) if "CLASS" in t ]

    plt.rcParams.update({'font.size': 15})
    fig = plt.figure(figsize=(4,4))
    ax = fig.add_subplot()
    plt.scatter(pres_x_other, pres_y_other, color="tab:green", s=15, marker = 'o', label = "Other Scenarios")
    plt.scatter(posts_x_class, posts_y_class, color="tab:green", s=50, marker = '*', label = "Classroom",edgecolors="black",linewidth=.5)
    plt.plot([0,1000], [0,1000], linestyle='dashed', linewidth=0.5, color="black")
    ax.set_aspect('equal', adjustable='box')
    plt.yscale("log")
    plt.xscale("log")
    plt.ylim((0.0001,2))
    plt.xlim((0.0001,2))
    plt.xticks([0.0001, 0.001, 0.01, 0.1, 1], fontsize=12)
    plt.yticks([0.0001, 0.001, 0.01, 0.1, 1], fontsize=12)
    plt.grid(color='black', linestyle='dashed', linewidth=0.3)
    plt.xlabel("Attack Rate\n(Kurnitski et al.)", fontsize=13)
    plt.ylabel("Attack Rate\n($AR^1$ - retQSS)")
    plt.legend(loc="lower right",prop={'size': 7})
    plt.tight_layout()
    plt.savefig("F3B.png")

Fig3A()
subprocess.run(["/home/estiben/Desktop/Proyectos/scripts/pngaddchar", "F3A.png", "a"])
Fig3B()
subprocess.run(["/home/estiben/Desktop/Proyectos/scripts/pngaddchar", "F3B.png", "b"])
subprocess.run(["montage", "-geometry", "400x400", "-tile", "1x2", "F3A.png", "F3B.png", "F3.png"])
subprocess.run(["rm", "F3A.png", "F3B.png"])
FigA1()
FigA2A()
subprocess.run(["/home/estiben/Desktop/Proyectos/scripts/pngaddchar", "FA2A.png", "a"])
FigA2B()
subprocess.run(["/home/estiben/Desktop/Proyectos/scripts/pngaddchar", "FA2B.png", "b"])
subprocess.run(["montage", "-geometry", "500x400", "-tile", "1x2", "FA2A.png", "FA2B.png", "FA2.png"])
subprocess.run(["rm", "FA2A.png", "FA2B.png"])
Fig4A()
subprocess.run(["/home/estiben/Desktop/Proyectos/scripts/pngaddchar", "F4A.png", "a"])
Fig4B()
subprocess.run(["/home/estiben/Desktop/Proyectos/scripts/pngaddchar", "F4B.png", "b"])
subprocess.run(["montage", "-geometry", "500x500", "-tile", "1x2", "F4A.png", "F4B.png", "F4.png"])
subprocess.run(["rm", "F4A.png", "F4B.png"])
Fig5A()
subprocess.run(["/home/estiben/Desktop/Proyectos/scripts/pngaddchar", "F5A.png", "a"])
Fig5B()
subprocess.run(["/home/estiben/Desktop/Proyectos/scripts/pngaddchar", "F5B.png", "b"])
subprocess.run(["montage", "-geometry", "500x500", "-tile", "1x2", "F5A.png", "F5B.png", "F5.png"])
subprocess.run(["rm", "F5A.png", "F5B.png"])




