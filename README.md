This repository stores data related to the paper entitled "A multi-scale agent-based model of aerosol-mediated indoor infections in heterogeneous scenarios"

To produce the figures the following command must be used:

```
python figures.py
```

Results were obteined using the software suite retQSS found in gitlab. Software can be downloaded using:

```
git clone https://gitlab.com/lukius/retqss.git
```

